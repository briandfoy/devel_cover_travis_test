use Test::More 1.0;

use_ok( 'Foo' );

pass() for 1 .. 3;


my $file = '/home/travis/perl5/perlbrew/perls/5.8/lib/site_perl/5.8.9/x86_64-linux/Devel/Cover.pm';
my $data = do { local(@ARGV, $/) = $file; <> }

my( $version_line ) = $data =~ m/^our \$VERSION/m;
diag( "Devel::Cover version: $version_line");


done_testing();
